import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:shared_preferences/shared_preferences.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});
  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  // static const _channel = EventChannel("com.example.app/smsStream");
  static const _sendSmsChannel = MethodChannel("com.example.sms_forwarder/sms");
  List<String> _forwardNumbers = [];
  bool _isListening = false;
  TextEditingController numbersController = TextEditingController();
  final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

  @override
  void initState() {
    super.initState();
    getNumberList();
    getPermission().then((value) {
      print("Permission granted $value");
    });
  }

  Future<void> getNumberList() async {
    final SharedPreferences prefs = await _prefs;
    List<String>? numbers = prefs.getStringList('numbersList');
    print("numbers to get $numbers");
    numbersController.text = numbers?.join(',') ?? '';
  }

  Future<bool> validateAndSaveNumbersToPrefs() async {
    final SharedPreferences prefs = await _prefs;
    String numbersString = numbersController.text.replaceAll(' ', '');
    _forwardNumbers = numbersString.split(',');
    print("numbers to set $_forwardNumbers");
    for (var number in _forwardNumbers) {
      if (!(number.length == 13 && number.startsWith('+91'))) {
        ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
          content: Text("Please enter valid numbers"),
        ));
        return false;
      }
    }
    prefs.setStringList('numbersList', _forwardNumbers);
    return true;
  }

  Future<bool> _startListening() async {
    if (!(await validateAndSaveNumbersToPrefs())) {
      return false;
    }
    if (!_isListening) {
      try {
        bool result = await _sendSmsChannel
            .invokeMethod('startListening', {'phoneNumbers': _forwardNumbers});
        setState(() {
          _isListening = true;
        });
        // _sendSmsChannel.setMethodCallHandler((call) async {
        //   if (call.method == 'onSmsReceived') {
        //     String message = call.arguments['message'];
        //     _forwardSms(message);
        //   }
        // });
        return result;
      } on PlatformException catch (e) {
        print("Failed to start listening: '${e.message}'.");
        return false;
      }
    }
    return false;
  }

  Future<bool> _stopListening() async {
    try {
      return await _sendSmsChannel.invokeMethod('stopListening');
    } on PlatformException catch (e) {
      print("Failed to stop listening: '${e.message}'.");
      return false;
    }
  }

  // void _forwardSms(String message) async {
  //   final SharedPreferences prefs = await _prefs;
  //   List<String>? forwardNumbers = prefs.getStringList('numbersList');
  //   for (String number in forwardNumbers ?? []) {
  //     try {
  //       await _sendSmsChannel
  //           .invokeMethod('sendSms', {'message': message, 'number': number});
  //     } on PlatformException catch (e) {
  //       print("Failed to send SMS: '${e.message}'.");
  //     }
  //   }
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("SMS"),
        backgroundColor: Colors.lightGreen,
        centerTitle: true,
      ),
      body: Center(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              const Text(
                "We are listening for SMS...",
                style: TextStyle(fontSize: 20),
              ),
              const Padding(
                padding: EdgeInsets.all(30.0),
                child: Text(
                  "Note: Add +91 country code. For multiple numbers add comma (,) seperation",
                  style: TextStyle(fontSize: 12),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 30.0),
                child: TextFormField(
                  controller: numbersController,
                ),
              ),
              const SizedBox(
                height: 50,
              ),
              Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  ElevatedButton(
                      onPressed: () async {
                        final bool result = await _startListening();
                        print("start_service $result");
                        if (result) {
                          ScaffoldMessenger.of(context)
                              .showSnackBar(const SnackBar(
                            content: Text("Service running..."),
                          ));
                        } else {
                          ScaffoldMessenger.of(context)
                              .showSnackBar(const SnackBar(
                            content: Text("Service already running..."),
                          ));
                        }
                      },
                      child: const Text("Start Service")),
                  const SizedBox(
                    height: 10,
                  ),
                  ElevatedButton(
                      onPressed: () async {
                        final bool result = await _stopListening();
                        print("stop_service $result");
                        if (result) {
                          ScaffoldMessenger.of(context)
                              .showSnackBar(const SnackBar(
                            content: Text("Service stopped..."),
                          ));
                        } else {
                          ScaffoldMessenger.of(context)
                              .showSnackBar(const SnackBar(
                            content: Text("Something went wrong..."),
                          ));
                        }
                      },
                      child: const Text("Stop Service"))
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  Future<bool> getPermission() async {
    if (await Permission.sms.status == PermissionStatus.granted) {
      return true;
    } else {
      if (await Permission.sms.request() == PermissionStatus.granted) {
        return true;
      } else {
        return false;
      }
    }
  }
}
