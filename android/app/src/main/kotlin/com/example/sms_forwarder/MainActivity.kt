package com.example.sms_forwarder
import android.app.*
import android.content.*
import android.graphics.Color
import android.os.Build
import android.os.IBinder
import android.provider.Telephony
import android.telephony.SmsManager
import android.telephony.SmsMessage
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat.getSystemService
import io.flutter.embedding.android.FlutterActivity
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugin.common.EventChannel
import io.flutter.plugin.common.MethodChannel

class MainActivity: FlutterActivity(){
    // private val eventChannel = "com.example.app/smsStream"
    @RequiresApi(Build.VERSION_CODES.O)
    override fun configureFlutterEngine(flutterEngine: FlutterEngine) {
        super.configureFlutterEngine(flutterEngine)
// EventChannel(flutterEngine.dartExecutor.binaryMessenger, eventChannel)
        var foregroundService: Intent
        MethodChannel(flutterEngine.dartExecutor.binaryMessenger,"com.example.sms_forwarder/sms").setMethodCallHandler {
                call, result ->
            when (call.method) {
                "startListening" -> {
                    val phoneNumbers = call.argument<ArrayList<String>>("phoneNumbers")
                    Log.d("TEST", phoneNumbers.toString());
                    val sharedPreferences = getSharedPreferences("sms_forwarder_prefs", Context.MODE_PRIVATE)
                    val editor = sharedPreferences.edit()
                    editor.putStringSet("phoneNumbers", phoneNumbers?.toSet() )
                    editor.apply()
                    foregroundService = Intent(
                        this,
                        SmsBackgroundService::class.java
                    ).apply {
                        putStringArrayListExtra("phoneNumbers", ArrayList(phoneNumbers))
                    }
                    startForegroundService(foregroundService)
                    result.success(true)
                }
                "sendSms" -> {
                    val phoneNumbers:ArrayList<String> = call.argument("phoneNumber")!!
                    val message: String = call.argument("message")!!
                    sendSMS(phoneNumbers, message)
                    result.success(true)
                }
                "stopListening" -> {
                    foregroundService = Intent(
                        this,
                        SmsBackgroundService::class.java
                    )
                    stopService(foregroundService)
                    result.success(true)
                }
                else -> {
                    result.notImplemented()
                    result.success(false)
                }
            }
        }
    }
    @RequiresApi(Build.VERSION_CODES.M)
    private fun sendSMS(phoneNumbers: ArrayList<String>, message: String) {
        val sentPI: PendingIntent = PendingIntent.getBroadcast(this, 0, Intent("SMS_SENT"),
            PendingIntent.FLAG_IMMUTABLE)
        for (number in phoneNumbers) {
            Log.d("SMS--FORWARD",number)
            SmsManager.getDefault().sendTextMessage(number, null, message, sentPI, null)
        }
    }
}
class SmsBackgroundService : Service(){
    private var phoneNumbers: List<String> = listOf()

    val smsReceiver = SmsReceiver()
    override fun onBind(intent: Intent?): IBinder? {
        intent?.let {
            phoneNumbers = it.getStringArrayListExtra("phoneNumbers")?.toList() ?: listOf()
            Log.d("onBind",phoneNumbers.toString())
        }
        return null;
    }
    @RequiresApi(Build.VERSION_CODES.O)
    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {

        intent?.let {
            phoneNumbers = it.getStringArrayListExtra("phoneNumbers")?.toList() ?: listOf()
            Log.d("ONSTARTCOMMAND",phoneNumbers.toString())
        }
        val notification = createNotification()
        startForeground(ONGOING_NOTIFICATION_ID, notification)
        val filter = IntentFilter("android.provider.Telephony.SMS_RECEIVED")
        filter.priority = 2147483647
        registerReceiver(smsReceiver, filter)?.apply {
            putStringArrayListExtra("phoneNumbers", ArrayList(phoneNumbers))
        }
        return super.onStartCommand(intent, flags, startId);
    }
    override fun onDestroy() {
        super.onDestroy()
        Log.d("SMS-FORWARD", "Service stopped")
        unregisterReceiver(smsReceiver)
    }
    @RequiresApi(Build.VERSION_CODES.M)
    private fun createNotification(): Notification {
// Create notification channel if API level is 26 or higher
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createNotificationChannel()
        }
        val notificationIntent = Intent(this, MainActivity::class.java)
        val pendingIntent = PendingIntent.getActivity(this, 1, notificationIntent,
            PendingIntent.FLAG_IMMUTABLE)
        return NotificationCompat.Builder(this, CHANNEL_ID)
            .setContentTitle("SMS Service")
            .setContentText("Service is running")
            .setSmallIcon(R.mipmap.ic_launcher)
            .setContentIntent(pendingIntent)
            .build()
    }
    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotificationChannel() {
        val channelName = "SMS Service Channel"
        val importance = NotificationManager.IMPORTANCE_DEFAULT
        val channel = NotificationChannel(CHANNEL_ID, channelName, importance)
        channel.description = "Channel for SMS Service"
        channel.enableLights(true)
        channel.lightColor = Color.BLUE
        val notificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.createNotificationChannel(channel)
    }
    companion object {
        private const val CHANNEL_ID = "SMSServiceChannel"
        private const val ONGOING_NOTIFICATION_ID = 1
    }
}
class SmsReceiver : EventChannel.StreamHandler, BroadcastReceiver(){

    var eventSink: EventChannel.EventSink? = null
    override fun onListen(arguments: Any?, events: EventChannel.EventSink?) {
        eventSink = events
    }
    override fun onCancel(arguments: Any?) {
        eventSink = null
    }
    @RequiresApi(Build.VERSION_CODES.M)
    override fun onReceive(context: Context?, intent: Intent?) {
        val sharedPreferences = context?.getSharedPreferences("sms_forwarder_prefs", Context.MODE_PRIVATE)
        val phoneNumbers = sharedPreferences?.getStringSet("phoneNumbers", emptySet())?.toList()

        Log.d("onReceive",phoneNumbers.toString())

        if (intent?.action == Telephony.Sms.Intents.SMS_RECEIVED_ACTION) {
            val bundle = intent.extras
            if (bundle != null) {
                val pdus = bundle.get("pdus") as Array<*>
                val messages = arrayOfNulls<SmsMessage>(pdus.size)
                for (i in pdus.indices) {
                    messages[i] = SmsMessage.createFromPdu(pdus[i] as ByteArray)
                }
                val messageBody = StringBuilder()
                for (message in messages) {
                    messageBody.append(message?.messageBody)
                }
                Log.d("SMS --FORWARD", messageBody.toString())
                Log.d("NUMBERS --FORWARD", phoneNumbers.toString())
                phoneNumbers?.forEach {
                    sendSMS(context,arrayOf(it), messageBody.toString())
                };
            }
        }
// for (sms in Telephony.Sms.Intents.getMessagesFromIntent(p1)) {
//// eventSink?.success(sms.displayMessageBody)
// Log.d("SMS--FORWARD",sms.displayMessageBody)
// Log.d("SMS--FORWARD",sms.messageBody)
//
// sendSMS(arrayOf("+919604879954"),sms.displayMessageBody)
//
// }
    }
    @RequiresApi(Build.VERSION_CODES.M)
    private fun sendSMS(context: Context, phoneNumbers: Array<String>, message: String) {
        if (message.contains("pack valid till",ignoreCase = true)) return;
        for (number in phoneNumbers) {
            Log.d("SMS--FORWARD",number)
           val message1 = message.replace(" ","") //remove if forwarding doesn't work for specific sms
            Log.d("SMS--FORWARD",message1)
//            context.getSystemService(SmsManager::class.java).sendTextMessage(number, null, "---FORWARDED--- \n$message1", null, null)
            val messageParts = context.getSystemService(SmsManager::class.java).divideMessage(message)
            messageParts.add(0, "---FORWARDED---\n")
            context.getSystemService(SmsManager::class.java).sendMultipartTextMessage(number, null, messageParts, null, null)

        }
    }
}